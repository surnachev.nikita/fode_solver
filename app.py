from utils import show_message, stylize_plot, get_solutions
from solver.entities import SolverParams, SolverState
from dash.dependencies import Input, Output
from solver.entities import SolverSolution
import plotly.graph_objects as go
from validation import Validator, ParametrizedGTE
from copy import deepcopy
from typing import List
from dash import html
from dash import dcc
from inputs import F, SOLVERS
import dash

app = dash.Dash(
    "Computational practicum",
    external_stylesheets=["/static/main.css"]
)


def get_solutions_plot(solutions: List[SolverSolution]) -> go.Figure:
    plots = []
    for sol in solutions:
        points = list(sol.points)
        plots.append(go.Scatter(
            name=sol.method,
            x=[p.x for p in points],
            y=[p.y for p in points],
        ))
    return stylize_plot(go.Figure(data=plots), title="Solutions")


def get_lte_plot(solutions: List[SolverSolution]) -> go.Figure:
    validator = Validator(solutions[0])
    benchmarks = validator.validate_batch(solutions[1:])
    plots = [go.Scatter(name=b.method, x=validator.exact_x, y=b.lte) for b in benchmarks]
    return stylize_plot(go.Figure(data=plots), title="LTE")


def get_gte_plot(solutions: List[SolverSolution]) -> go.Figure:
    validator = Validator(solutions[0])
    benchmarks = list(validator.validate_batch(solutions[1:]))
    return stylize_plot(go.Figure(data=[
        go.Bar(
            x=[b.method for b in benchmarks],
            y=[b.gte for b in benchmarks])
    ]), title="GTE for N")


app.layout = html.Div(children=[
    html.H1(children="Computational practicum"),
    html.H2(children="Nikita Surnachev. Variant 6"),
    html.Div(children="Solves y = 2x(x^2 + y) with initial parameters x0, y0, X and N"),
    html.Br(),
    dcc.Input(id="x0", type="text", placeholder="x0"),
    dcc.Input(id="y0", type="text", placeholder="y0"),
    dcc.Input(id="X", type="text", placeholder="X"),
    dcc.Input(id="N", type="text", placeholder="N"),
    dcc.Graph(id="solutions", figure=show_message("Initializing...")),
    dcc.Graph(id="lte", figure=show_message("Initializing...")),
    dcc.Graph(id="gte", figure=show_message("Initializing...")),

    html.Hr(),
    dcc.Input(id="n0", type="text", placeholder="n0"),
    dcc.Graph(id="parametrized_gte", figure=show_message("Initializing..."))
])


@app.callback(
    Output("solutions", "figure"),
    Output("lte", "figure"),
    Output("gte", "figure"),
    Input("x0", "value"),
    Input("y0", "value"),
    Input("X", "value"),
    Input("N", "value"),
)
def update_solutions_callback(x0: str, y0: str, X: str, N: str):
    try:
        solver_params = SolverParams(initial_state=SolverState(x=x0, y=y0), X=X, N=N, f=F)
    except ValueError:
        return [show_message("Invalid input data") for _ in range(3)]

    solutions = list(get_solutions(solver_params))
    return [get_solutions_plot(deepcopy(solutions)),
            get_lte_plot(deepcopy(solutions)),
            get_gte_plot(solutions)]


@app.callback(
    Output("parametrized_gte", "figure"),
    Input("x0", "value"),
    Input("y0", "value"),
    Input("X", "value"),
    Input("N", "value"),
    Input("n0", "value")
)
def update_gte_callback(x0: str, y0: str, X: str, N: str, n0: str):
    n0 = n0 if n0 else "0"
    if not (N and n0.isdigit() and N.isdigit() and int(n0) <= int(N)):
        return show_message("Invalid input data")
    try:
        solver_params = SolverParams(initial_state=SolverState(x=x0, y=y0), X=X, N=N, f=F)
    except ValueError:
        return show_message("Invalid input data")

    validator = ParametrizedGTE(solver_params, (int(n0), int(N)))
    parametrized_gte = validator.validate()
    X = [gte.N for gte in parametrized_gte]

    plots = []
    for solver in SOLVERS[1:]:
        method = solver.__name__
        plots.append(go.Scatter(
            name=method,
            x=X,
            y=[next(filter(lambda x: x.method == method, gte.benchmarks)).gte for gte in parametrized_gte],
        ))
    return stylize_plot(go.Figure(data=plots), title="Parametrized GTE")


if __name__ == "__main__":
    app.run_server(debug=False, host="0.0.0.0")
