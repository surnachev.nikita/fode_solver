FROM python:3.6

EXPOSE 8050

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

COPY . /app
WORKDIR /app
CMD ["python", "app.py"]