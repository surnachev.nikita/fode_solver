import copy
from typing import Sequence, List, Tuple, Iterator
from solver.entities import SolverSolution, SolverParams
from utils import get_solutions
from pydantic import BaseModel
from typing import Tuple


def calculate_lte(predictions: Sequence[float], targets: Sequence[float]) -> List[float]:
    """
    Calculates local truncation error
    :param predictions: y-values of numerical solution
    :param targets: y-values of exact solution
    :return: lte error
    """
    return [abs(p - t) for p, t in zip(predictions, targets)]


def calculate_gte(predictions: Sequence[float], targets: Sequence[float]) -> float:
    """
    Calculates global truncation error
    :param predictions: y-values of numerical solution
    :param targets: y-values of exact solution
    :return: gte error on all X-s
    """
    return max(calculate_lte(predictions, targets))


class Benchmark(BaseModel):
    """Metrics of one solution"""
    method: str  # method name
    lte: Sequence[float]  # local truncation error
    gte: float  # global truncation error


class ParametrizedBenchmark(BaseModel):
    """ Parametrized metrics of the solution """
    N: int  # number of grid cells
    benchmarks: List[Benchmark]  # benchmarks calculated with given parameter (N)


class Validator:
    """ Calculates benchmarks for multiple solutions """

    @staticmethod
    def _solution_to_coords(solution: SolverSolution) -> Tuple[List[float], List[float]]:
        points = list(solution.points)
        solution_x = [p.x for p in points]
        solution_y = [p.y for p in points]
        return solution_x, solution_y

    def __init__(self, exact_solution: SolverSolution):
        self.exact_x, self.exact_y = self._solution_to_coords(exact_solution)

    def validate(self, solution: SolverSolution) -> Benchmark:
        solution_x, solution_y = self._solution_to_coords(solution)
        assert solution_x == self.exact_x, "Arguments are not equal!"
        return Benchmark(
            method=solution.method,
            lte=calculate_lte(self.exact_y, solution_y),
            gte=calculate_gte(self.exact_y, solution_y)
        )

    def validate_batch(self, solutions: Sequence[SolverSolution]) -> Iterator[Benchmark]:
        return map(lambda x: self.validate(x), solutions)


class ParametrizedGTE:
    """ Calculates GTE for different numbers of grids """

    def __init__(self, initial_params: SolverParams, n_range: Tuple[int, int]):
        self.n_range = n_range
        self.initial_params = initial_params

    def _get_solutions_with_n(self, n: int) -> List[SolverSolution]:
        params = copy.copy(self.initial_params)
        params.N = n
        return list(get_solutions(params))

    @staticmethod
    def _get_gte(solutions: List[SolverSolution]) -> List[Benchmark]:
        return list(Validator(solutions[0]).validate_batch(solutions[1:]))

    def validate(self) -> List[ParametrizedBenchmark]:
        return [ParametrizedBenchmark(N=n,
                                      benchmarks=self._get_gte(self._get_solutions_with_n(n)))
                for n in range(*self.n_range)]
