from solver.entities import SolverParams, SolverSolution
import plotly.graph_objects as go
from typing import Iterator
from inputs import SOLVERS


def get_solutions(solver_parameters: SolverParams) -> Iterator[SolverSolution]:
    """
    Runs all solvers with given initial parameters
    :param solver_parameters: common parameters for all solvers
    :return: iterator with solutions
    """
    solvers = map(lambda x: x(initial_params=solver_parameters), SOLVERS)
    solutions = map(lambda x: x.solve(), solvers)
    return solutions


def stylize_plot(fig: go.Figure, title="") -> go.Figure:
    """
    Updates styles and title of the figure
    :param fig: some figure
    :param title: new title for the given figure
    :return: updated figure
    """
    fig.update_layout(
        plot_bgcolor="black",
        paper_bgcolor="black",
        font_color="#7FDBFF",
        title=title
    )
    return fig


def show_message(message: str) -> go.Figure:
    """
    Fraws figure with message in the center
    :param message: text of the message
    :return: figure
    """
    fig = go.Figure()
    fig.update_layout(
        xaxis={"visible": False},
        yaxis={"visible": False},
        annotations=[
            {
                "text": message,
                "xref": "paper",
                "yref": "paper",
                "showarrow": False,
                "font": {
                    "size": 30
                }
            }
        ]
    )
    return stylize_plot(fig)
