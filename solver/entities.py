from pydantic import BaseModel, validator
from typing import Callable, Sequence, Optional


class SolverState(BaseModel):
    """ Iterator iterm """
    x: float  # x-coord for approximated point
    y: float  # y-coord for approximated point
    iteration: Optional[int] = 0


class SolverParams(BaseModel):
    """ Common solver initial params """
    initial_state: SolverState  # initialize with IVP point
    X: float  # upper bound for y(x) arguments
    N: int    # number of points
    f: Callable[[float, float], float]  # derivative of y (y' = f)

    @validator("X")
    def arg_range_validator(cls, X, values):
        if X <= values["initial_state"].x:
            raise ValueError("Invalid range")
        return X

    @validator("N")
    def n_is_positive(cls, val):
        if val <= 0:
            raise ValueError("N must be positive number")
        return val


class SolverSolution(BaseModel):
    """ Solver output """
    method: str  # name of the solver method
    initial_params: SolverParams  # initial parameters with which the answer was calculated
    points: Sequence[SolverState]  # approximated points
