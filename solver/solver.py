from abc import ABC, abstractmethod
from solver.entities import SolverParams, SolverState, SolverSolution


class IterativeSolver(ABC):
    """
    Abstract class that defines interface to the methods for solving IVP
    """

    def __init__(self, initial_params: SolverParams):
        """
        :param initial_params: initial params for differential equation
        """
        self.initial_params = initial_params

    def __iter__(self):
        previous_state = self.initial_params.initial_state
        yield previous_state
        for it in range(self.initial_params.N):
            previous_state = self.next(previous_state)
            yield previous_state

    def _get_h(self) -> float:
        return (self.initial_params.X - self.initial_params.initial_state.x) / self.initial_params.N

    def solve(self) -> SolverSolution:
        """ Returns solution """
        return SolverSolution(
            method=self.__class__.__name__,
            initial_params=self.initial_params,
            points=iter(self)
        )

    @abstractmethod
    def next(self, state: SolverState) -> SolverState:
        """
        Solves the given differential equation with initial params
        :param state: current state of the solver
        :return: solution for the differential equation
        """
        return NotImplemented
