from .entities import SolverParams, SolverState
from .solver import IterativeSolver
from typing import Callable


class ExactSolver(IterativeSolver):
    def __init__(self, initial_params: SolverParams,
                 y: Callable[[float, float], float],
                 c: Callable[[float, float], float]):
        super().__init__(initial_params)
        self.y = y
        self.c = c

    def next(self, state: SolverState) -> SolverState:
        return SolverState(
            x=state.x + self._get_h(),
            y=self.y(state.x + self._get_h(), self.c(
                self.initial_params.initial_state.x,
                self.initial_params.initial_state.y)),
            iteration=state.iteration + 1
        )
