from .entities import SolverState
from .solver import IterativeSolver


class ImprovedEulerSolver(IterativeSolver):
    def next(self, state: SolverState) -> SolverState:
        next_x = state.x + self._get_h()
        k1 = self._get_h() * self.initial_params.f(state.x, state.y)
        k2 = self._get_h() * self.initial_params.f(next_x, state.y + k1)
        return SolverState(
            x=next_x,
            y=state.y + 0.5 * (k1 + k2),
            iteration=state.iteration + 1
        )
