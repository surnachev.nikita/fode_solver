from .entities import SolverState
from .solver import IterativeSolver


class VanillaEulerSolver(IterativeSolver):
    def next(self, state: SolverState) -> SolverState:
        return SolverState(
            x=state.x + self._get_h(),
            y=state.y + self._get_h() * self.initial_params.f(state.x, state.y),
            iteration=state.iteration + 1
        )
