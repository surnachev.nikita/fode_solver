from .entities import SolverState
from .solver import IterativeSolver


class RungeKuttaSolver(IterativeSolver):
    def next(self, state: SolverState) -> SolverState:
        next_x = state.x + self._get_h()
        k1 = self.initial_params.f(state.x, state.y)
        k2 = self.initial_params.f(state.x + self._get_h() / 2, state.y + (self._get_h() / 2) * k1)
        k3 = self.initial_params.f(state.x + self._get_h() / 2, state.y + (self._get_h() / 2) * k2)
        k4 = self.initial_params.f(next_x, state.y + self._get_h() * k3)
        return SolverState(
            x=next_x,
            y=state.y + (self._get_h() / 6) * (k1 + 2*k2 + 2*k3 + k4),
            iteration=state.iteration + 1
        )
