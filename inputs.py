from solver.runge_kutta_solver import RungeKuttaSolver
from solver.improved_euler import ImprovedEulerSolver
from solver.vanilla_euler import VanillaEulerSolver
from solver.exact_solver import ExactSolver
from functools import partial
import math


def F(x: float, y: float) -> float:
    """ F = y' is a First Order Differential Equation """
    return 2 * x * (x ** 2 + y)


def Y(x: float, c: float) -> float:
    """ Solved differential equation F """
    return c * math.exp(x**2) - x**2 - 1


def C(x0: float, y0: float) -> float:
    """ Constant expressed from Y """
    return (y0 + x0**2 + 1) / math.exp(x0**2)


# List of solvers for comparison in GUI
# Includes ExactSolver as a first element
SOLVERS = [partial(ExactSolver, y=Y, c=C)] + [VanillaEulerSolver, ImprovedEulerSolver, RungeKuttaSolver]
