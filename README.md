# First Order Differential Equation Solver 
Nikita Surnachev. Variant 6

Repository: https://gitlab.com/surnachev.nikita/fode_solver

Solves FODE with different numerical methods (Euler, Improved Euler, Runge-Kutta) and compares them. For testing purposes equation $F = 2x(x^2 + y)$ used. But you can easily change it (for now no user input foreseen, you can just change function $F$ in inputs.py module)

### Exact solution
Exact solution is necessary for benchmarking different methods. 
![](https://i.imgur.com/tDqmHgI.png)
![](https://i.imgur.com/alKj9m3.png)
If you want to change $F$, you also need to specify functions $Y$ and $C$ in inputs.py which is a solution of the Initial Value Problem


### UML Diagram

![](https://i.imgur.com/APQOBUb.png)



### Usage
1) **Docker** 
You can run project in Docker. Just run ```docker build -t fode_solver .``` in project directory.
Then run ``` docker run -p 8050:8050 fode_solver ```.
Go to the localhost:8050

2) **Pip**
Intall python dependencies: ```pip install -r requirements.txt```
Run: ```python app.py```


### Project dependencies
```
plotly==5.3.1
dash==2.0.0
pydantic==1.8.2
```

**Plotly** is used for creating interactive plots
**Dash** is used for serving plotly dynamic figures
**Pydantic** is used for validating and converting I/O

### Generally code could be divided in three main parts:
- Solvers
- Server
- Metrics

In solvers implemented three different numerical methods
In server callbacks and hosting are implemented 
In metrics Validation and GTE calculating implemented

### Screenshots
![](https://i.imgur.com/0kJcXbd.png)
![](https://i.imgur.com/6YArMXU.png)
![](https://i.imgur.com/sJOIALZ.png)
![](https://i.imgur.com/VLHv8Q2.png)

### Some code 

```
class IterativeSolver(ABC):
    """
    Abstract class that defines interface to the methods for solving IVP
    """

    def __init__(self, initial_params: SolverParams):
        """
        :param initial_params: initial params for differential equation
        """
        self.initial_params = initial_params

    def __iter__(self):
        previous_state = self.initial_params.initial_state
        yield previous_state
        for it in range(self.initial_params.N):
            previous_state = self.next(previous_state)
            yield previous_state

    def _get_h(self) -> float:
        return (self.initial_params.X - self.initial_params.initial_state.x) / self.initial_params.N

    def solve(self) -> SolverSolution:
        """ Returns solution """
        return SolverSolution(
            method=self.__class__.__name__,
            initial_params=self.initial_params,
            points=iter(self)
        )

    @abstractmethod
    def next(self, state: SolverState) -> SolverState:
        """
        Solves the given differential equation with initial params
        :param state: current state of the solver
        :return: solution for the differential equation
        """
        return NotImplemented
```

Solver is implemented as AbstractClass which implements iterator interface so it could be beautifully used with laziness and other pythonic stuff.

So, implementation of particular method is very compact:
```
class VanillaEulerSolver(IterativeSolver):
    def next(self, state: SolverState) -> SolverState:
        return SolverState(
            x=state.x + self._get_h(),
            y=state.y + self._get_h() * self.initial_params.f(state.x, state.y),
            iteration=state.iteration + 1
        )
```


Then, in inputs.py you can easily chose methods for testing:
```
SOLVERS = [partial(ExactSolver, y=Y, c=C)] + [VanillaEulerSolver, ImprovedEulerSolver, RungeKuttaSolver]
```

All I/O entities are implemented as a Pydantic.BaseModel like this:

```
class SolverState(BaseModel):
    """ Iterator iterm """
    x: float  # x-coord for approximated point
    y: float  # y-coord for approximated point
    iteration: Optional[int] = 0
```

```
try:
    solver_params = SolverParams(initial_state=SolverState(x=x0, y=y0), X=X, N=N, f=F)
except ValueError:
    return show_message("Invalid input data")
```
